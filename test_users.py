import requests


def test_get_news():
    url = "https://back.slkrug.ru/api/v1/news/"
    response = requests.request("GET", url)
    assert response.status_code == 200

def test_get_picture():
    url = "https://back.slkrug.ru/media/collection_images/2023/09/01/C0nbl2Qpiw8.jpg"
    response = requests.request("GET", url)
    assert response.status_code == 200
